/*
 * Copyright (C) 2022 Pierre Gnaedig
 * bbswicth FreeBSD kernel module
 *
 * Copyright (c) 1998, Regents of the University of California
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  * Neither the name of the University of California, Berkeley nor the
 *  names of its contributors may be used to endorse or promote products
 *  derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * *WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * NCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * SS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * NCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <sys/types.h>
#include <sys/systm.h>
#include <sys/param.h>
#include <sys/conf.h>
#include <sys/sysctl.h>
#include <sys/kernel.h>
#include <sys/module.h>

static int8_t bbswitch_state = 0;
static int bbswitch_oid_handler(struct sysctl_oid *oidp,
				void *arg1,
				intmax_t arg2,
				struct sysctl_req *reqp);
static int bbswitch_module_handler(module_t mod, int what, void* arg);

static moduledata_t bbswitch_moddata = {
  "bbswitch",
  bbswitch_module_handler,
  NULL
};

int
bbswitch_oid_handler(struct sysctl_oid *oidp,
		     void *arg1, intmax_t arg2,
		     struct sysctl_req *reqp)
{
  if (oidp) {
    bbswitch_state = 1;
    return 0;
  }
  else
    return -1;
}

int
bbswitch_module_handler(module_t mod, int what, void * arg)
{
  switch (what) {
  case MOD_LOAD:
    printf("bbswitch module loaded.\n");
    break;
  case MOD_UNLOAD:
    printf("bbswitch module unloaded.\n");
    break;
  default:
    return EOPNOTSUPP;
  }
  
  return 0;
}

SYSCTL_DECL(bbswitch);
SYSCTL_PROC(_hw, OID_AUTO, bbswitch, CTLTYPE_U8 | CTLFLAG_RW,
	    NULL, 0, &bbswitch_oid_handler, "UI", "Enable bbswitch");

MODULE_VERSION(bbswitch, 1);
DECLARE_MODULE(bbswitch, bbswitch_moddata, SI_SUB_EXEC, SI_ORDER_ANY);
