# Copyright (C) 2022 Pierre Gnaedig
# All rights reserved.

# bbswitch

KMOD=	bbswitch
SRCS=	bbswitch.c

.include <bsd.kmod.mk>
